#include <errno.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define KB (size_t)(1024)
#define MB (size_t)(KB * 1024)
#define GB (size_t)(MB * 1024)


int main()
{
  double r = sqrt(-1);
  printf("%lf\n", r);
  if (errno) {
    fprintf(stderr, "Error: %d [%s]\n", errno, strerror(errno));
    errno = 0;
  }

  double undef = 1./0.;
  printf("%lf\n", undef);
  if (errno) {
    printf("Error: %d [%s]\n", errno, strerror(errno));
    errno = 0;
  }

  FILE *file = fopen("super.txt", "w");
  if (!file) {
    printf("Error: %d [%s]\n", errno, strerror(errno));
    errno = 0;
  } else fclose(file);

  void *mem = malloc(10 * GB);
  if (!mem) {
    //  printf("Error: %d [%s]\n", errno, strerror(errno));
    perror("Erro");
    errno = 0;
  } else free(mem);

  // Atenção: overflow não é detectado pelo sistema.
  unsigned int ui = 0; --ui;
  int i = 1000000, j = 1000000;
  int ij = i * j;
  printf("%d\n", ij);
  printf("Error: %d [%s]\n", errno, strerror(errno));

}
