#include <stdarg.h>
#include <stdio.h>

// Variadic functions.
// Funções que podem receber um número arbitrário de argumentos.
// É necessário declarar no mínimo 1 parametro fixo.
void minprintf(char *fmt, ...) // minimal printf: K&R p.156
{
  va_list ap; // ap = argument pointer.
  char *p, *sval;
  int ival;
  double dval;

  va_start(ap, fmt);
  for (p = fmt; *p; p++) {
    if (*p != '%') {
      putchar(*p);
      continue;
    }
    switch (*++p) {
    case 'd':
      ival = va_arg(ap, int);
      printf("%d", ival);
      break;
    case 'f':
      dval = va_arg(ap, double);
      printf("%f", dval);
      break;n
    case 's':
      for (sval = va_arg(ap, char *); *sval; sval++)
        putchar(*sval);
      break;
    default:
      putchar(*p);
      break;
    }
  }
  va_end(ap);
}


int *initialize_array(int len, ...)
{
  int *array = malloc(sizeof(*array) * len);
  va_list ap;

  va_start(ap, len);
  for (int i = 0; i < len; ++i) {

  }
  va_end(ap);
}

int main()
{
  int i = 5, j = 7;
  float f = 3.14;
  char *s = "string";
  minprintf("i = %d, f = %f, j = %d, s = %s\n", i, f, j, s);

//  int *array = initialize_array(5, 1, 2, 3, 4, 5);
}
