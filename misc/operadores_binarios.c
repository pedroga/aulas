#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define array_len(array) (sizeof(array) / sizeof(*array))

void print_bits(uint32_t bits)
{
  // Código...
}

// Escreva uma função que receba um valor em decimal e copie o valor para um outro número bit a bit.
//----------------------------------------------------------------------------------------------------------------------
void exercicio_1()
{
  uint32_t decimais[] = {0, 1, 2, 3, 5, 7, 8, 10, 30, 32}; // Números decimais de teste.
  for (int i = 0; i < array_len(decimais); ++i) {
    uint32_t binario = 0;
    uint32_t decimal = decimais[i];

    // Código...

    printf("D: Decimal %d = Binário %d\n", decimal, binario);
  }
}

// Escreva uma função que receba uma string de zeros e uns e crie um inteiro a com tal representação binária.
// Obs: ignore os espaços permitindo que a string contenha espaços para separar agrupamentos de bits.
// Exemplo: "1010" -> resultado = 10.
//----------------------------------------------------------------------------------------------------------------------
void exercicio_2()
{
  char *strings[] = {// Aviso: nesta implementação, o uso excessivo de espaços pode causar overflow.
    "0", "1", "1", "10", "11", "101", "1000", "1101", "1 0101", "10 0010", "11 0111", "101 1001", "1001 0000",
  };
  for (int i = 0; i < array_len(strings); ++i) {
    uint32_t binario = 0;

    // Código...

    printf("string = [%s] = %d\n", strings[i], binario);
    print_bits(binario);
  }
}

// Escreva uma função que faça o complemento de dois de um número.
//----------------------------------------------------------------------------------------------------------------------
void exercicio_3()
{
  int decimais[] = {1, 2, 3, 10, 100, 123456789, -1, -2, -3, -10, -100, -123456789};
  for (int i = 0; i < array_len(decimais); ++i) {
    int complemento = decimais[i];
    // O operador ~ faz o complemento de 1.

    // Código...

    printf("%d // %d\n", decimais[i], complemento);
  }
}

// Escreva uma função que seleciona n bits de um inteiro a partir de do i-ésimo bit, e transforma eles
// em um número decimal (use shift right para alinhar os bits selecionados ao primeiro bit.
// Exemplo: bits = 0b1110001011, i = 3, n = 5 -> Resultado = 0b10001 = 17
//----------------------------------------------------------------------------------------------------------------------
// Função auxiliar para criar uma mascara de n bits a partir do bit i.
uint32_t create_mask(int n, int i)
{
  uint32_t mask = 0;

  // Código.

  return mask;
}

// Função auxiliar para selecionar n bits a partir do bit i.
uint32_t select_bits(uint32_t bits, int n, int i)
{
  uint32_t result = 0;

  // Código...

  return result;
}

void exercicio_4()
{
  uint32_t binarios[] = {0b1, 0b1010, 0b1110001011, 0b10101010};
  struct {int n, i;} params[] = {
    {.n = 1, .i = 0},
    {.n = 3, .i = 1},
    {.n = 5, .i = 3},
    {.n = 4, .i = 4},
  };
  for (int i = 0; i < array_len(binarios); ++i) {
    uint32_t selected = select_bits(binarios[i], params[i].n, params[i].i);
    selected >>= params[i].i;
    puts("Original:");
    print_bits(binarios[i]);
    puts("Selecionado:");
    print_bits(selected);
  }
}

// Implemente o 15-puzzle usando um uint64_t como representação do quebra-cabeças inteiro.
//----------------------------------------------------------------------------------------------------------------------
typedef uint64_t Puzzle;

// Gera um quebra-cabeças aleatório.
Puzzle generate_random_puzzle();

// Move peça da esquerda do vazio. Exemplo: 1 0 2 3 -> 0 1 2 3.
Puzzle move_left(Puzzle p);

// Move peça da direita do vazio. Exemplo: 1 0 2 3 -> 1 2 0 3.
Puzzle move_right(Puzzle p);

// Move peça de cima do vazio. Exemplo:
// 4 5 6 7 -> 4 0 6 7
// 1 0 2 3 -> 1 5 2 3.
Puzzle move_up(Puzzle p);

// Move peça de baixo do vazio. Exemplo:
// 4 0 6 7 -> 4 5 6 7
// 1 5 2 3 -> 1 0 2 3.
Puzzle move_down(Puzzle p);

// Verifica se a solução está correta.
bool check_solved(Puzzle p);

// Imprime o quebra-cabeças no terminal no formato de uma matriz 4x4.
void print_puzzle(Puzzle p);

void puzzle_game()
{
/*
  Puzzle puzzle = generate_random_puzzle();
  while (!check_solved(puzzle)) {
    print_puzzle(puzzle);
    int c = getchar();
    switch (c) {
    case 'l': puzzle = move_left(puzzle);  break;
    case 'r': puzzle = move_right(puzzle); break;
    case 'u': puzzle = move_up(puzzle);    break;
    case 'd': puzzle = move_down(puzzle);  break;
    }
  }
*/
}

// Main.
//----------------------------------------------------------------------------------------------------------------------
int main()
{
//  exercicio_1();
//  exercicio_2();
//  exercicio_3();
//  exercicio_4();
//  puzzle_game();
}
