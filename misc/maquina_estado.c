#include <stdio.h>

enum Modo_jogo { MODO_MENU_PRINCIPAL, MODO_CRIA_PERSONAGEM, MODO_OPCOES, MODO_OVERWORLD, MODO_LUTA };
enum Evento { COMECA_JOGO = 0, CARREGA_JOGO, ENTRA_BATALHA, TERMINA_BATALHA, ENTRA_OPCOES, CONFIRMA_CRIACAO, CONFIRMA_OPCOES };

enum Evento get_evento()
{
  int i;
  scanf("%d", &i);
  return i;
}

int main()
{
  enum Modo_jogo estado = MODO_MENU_PRINCIPAL;
  enum Evento ev;
  while (1) {
    printf("Estado = %d\n", estado);
    switch (estado) {
    case MODO_MENU_PRINCIPAL:
      ev = get_evento();
      if (ev == COMECA_JOGO) estado = MODO_CRIA_PERSONAGEM;
      else if (ev == CARREGA_JOGO) estado = MODO_OVERWORLD;
      else if (ev == ENTRA_OPCOES) estado = MODO_OPCOES;
      else estado = -1;
      break;
    case MODO_CRIA_PERSONAGEM:
      ev = get_evento();
      if (ev == CONFIRMA_CRIACAO) estado = MODO_OVERWORLD;
      else estado = -1;
      break;
    case MODO_OPCOES:
      ev = get_evento();
      if (ev == CONFIRMA_OPCOES) estado = MODO_MENU_PRINCIPAL;
      else estado = -1;
      break;
    case MODO_OVERWORLD:
      ev = get_evento();
      if (ev == ENTRA_BATALHA) estado = MODO_LUTA;
      else estado = -1;
      break;
    case MODO_LUTA:
      ev = get_evento();
      if (ev == TERMINA_BATALHA) estado = MODO_OVERWORLD;
      else estado = -1;
      break;
    default:
      goto saida;
    }
  }
saida:
}
