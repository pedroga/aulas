void clear_stdin(void)
{
  while (getchar() != '\n')
    ;
}

void read_guess(int guess[g_passwd_sz], int try)
{
start_read:
  int idx = 0;
  printf("[%d]$ ", try);
  while (idx < g_passwd_sz) {
    char *p;
    if (scanf("%ms", &p) == 1) {
      int val = atoi(p);
      if (val > 0 && val <= g_max_color) {
        guess[idx++] = val;
      } else {
        printf("Invalid value [%s]\n", p);
        goto start_read;
      }
      free(p);
    }
  }
  clear_stdin();
}

// É claro que poderiamos determinar um tamanho máximo para passwd_str e alocar de uma só vez:
//   passwd_str = malloc(str_size = (count_digits(g_max_color) * g_passwd_sz + (2 * g_passwd_sz)));
// Este código que segue é um exemplo de como:
// 1. fazer um array crescer conforme necessário usando realloc;
// 2. usar o snprintf de forma segura (sem possibilidade de escrever fora do buffer);
char *get_passwd_str(int passwd[g_passwd_sz])
{
  size_t str_size = 0, str_len = 0;
  char *passwd_str = NULL;
  for (int i = 0; i < g_passwd_sz; ++i) {
    int new_len = str_len + count_digits(passwd[i]) + 2;
    if (new_len >= str_size) {
      passwd_str = realloc(passwd_str, str_size = new_len + 1);
    }
    str_len += snprintf(passwd_str + str_len, str_size - str_len, "[%d]", passwd[i]);
    assert(str_len == new_len);
  }
  return passwd_str;
}

void cli_play(int passwd[g_passwd_sz])
{
  int try = 1;
  char *passwd_str = get_passwd_str(passwd);
  do {
    int guess[g_passwd_sz];
    read_guess(guess, try);
    Hint hint = verify(passwd, guess);
    printf("Corretos: %d // Parciais: %d\n", hint.correct, hint.partial);
    if (hint.correct == g_passwd_sz) {
      printf("Parabens, você acertou a senha!\n");
      break;
    }
  } while (try++ < g_max_tries);
  if (try > g_max_tries) printf("Que pena! A senha era: %s\n", passwd_str);
  free(passwd_str);
}
