#include <assert.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

// Default.
int g_max_color = 8;
int g_max_tries = 8;
int g_passwd_sz = 4;
bool g_use_gui = false;

typedef struct {
  int correct;
  int partial;
} Hint;

Hint verify(int passwd[g_passwd_sz], int guess[g_passwd_sz])
{
  Hint result = {};
  int passwd_count[g_max_color + 1];

  // Limpa a memória do array auxiliar.
  memset(passwd_count, 0, (g_max_color + 1) * sizeof(*passwd_count));

  for (int i = 0; i < g_passwd_sz; ++i) // Conta quantas vezes cada 'cor' aparace na senha.
    ++passwd_count[passwd[i]];
  for (int i = 0; i < g_passwd_sz; ++i) // Verifica acertos.
    if (guess[i] == passwd[i]) {
      ++result.correct;
      --passwd_count[guess[i]];
    }
  for (int i = 0; i < g_passwd_sz; ++i) // Verifica parciais.
    if (passwd_count[guess[i]] > 0) {
      ++result.partial;
      --passwd_count[guess[i]];
    }

  return result;
}

void parse_opts(int argc, char **argv)
{
  int opt;
  while ((opt = getopt(argc, argv, "hc:n:t:g")) != -1) {
    switch (opt) {
    case 'h':
      printf("Opções:\n"
             "  -n <VAL> // Comprimento da senha\n"
             "  -c <VAL> // Quantidade de cores\n"
             "  -t <VAL> // Máximo de tentativas\n"
             "  -g       // Usa interface gráfica\n"
      );
      exit(1);
    case 'n': {
      g_passwd_sz = atoi(optarg);
    } break;
    case 'c': {
      g_max_color = atoi(optarg);
    } break;
    case 't': {
      g_max_tries = atoi(optarg);
    } break;
    case 'g': {
      g_use_gui = true;
    } break;
    }
  }
  printf(
    "As cores vão de 1 até %d\n"
    "O tamanho da senha é %d\n"
    "Você tem %d tentativas no máximo\n",
    g_max_color, g_passwd_sz, g_max_tries);
}

int count_digits(int val)
{
  int result = 1;
  while (val /= 10) ++result;
  return result;
}

void gen_passwd(int passwd[g_passwd_sz])
{
  for (int i = 0; i < g_passwd_sz; ++i)
    passwd[i] = 1 + (rand() % g_max_color);
}

#include "cli.c"
#include "gui.c"

int main(int argc, char **argv)
{
  int passwd[g_passwd_sz];

  srand(time(NULL));
  parse_opts(argc, argv);
  gen_passwd(passwd);
  if (g_use_gui) gui_play(passwd);
  else cli_play(passwd);
}
