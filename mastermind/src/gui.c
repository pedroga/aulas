#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_mixer.h>
#include <SDL2/SDL_ttf.h>

// SDL.
//----------------------------------------------------------------------------------------------------------------------
#define WINDOW_W 1024
#define WINDOW_H 968
#define SPHERE_DIM (WINDOW_W/16)
#define checked(cond, error_msg) {if (cond) { fprintf(stderr, "Error: %s\n", error_msg); abort(); }}

SDL_Window *g_window = NULL;
SDL_Renderer *g_renderer = NULL;
TTF_Font *g_font = NULL;

// The password.
int *g_passwd = NULL;

// Current guess.
int g_current_guess_len = 0;
int *g_current_guess = NULL;

// Guess grid.
int g_last_guess = 0;
SDL_Texture **g_guess_grid = NULL;

void gui_close(void)
{
  TTF_CloseFont(g_font);
  SDL_DestroyRenderer(g_renderer);
  SDL_DestroyWindow(g_window);
  Mix_Quit();
  TTF_Quit();
  IMG_Quit();
  SDL_Quit();
}

void gui_init(void)
{
  atexit(gui_close);
  checked(SDL_Init(SDL_INIT_EVERYTHING) < 0, SDL_GetError());
  checked(!(IMG_Init(IMG_INIT_PNG) & IMG_INIT_PNG), IMG_GetError());
  checked(TTF_Init() == -1, TTF_GetError());
  checked(Mix_OpenAudio( 44100, MIX_DEFAULT_FORMAT, 2, 2048 ) < 0, Mix_GetError());
  checked(!(g_window = SDL_CreateWindow(
              "Mastermind", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, WINDOW_W, WINDOW_H, SDL_WINDOW_SHOWN)),
          SDL_GetError());
  checked(!(g_renderer = SDL_CreateRenderer(
              g_window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC | SDL_RENDERER_TARGETTEXTURE)),
          SDL_GetError());
  checked((SDL_SetRenderDrawBlendMode(g_renderer, SDL_BLENDMODE_BLEND)), SDL_GetError());
  checked(!(g_font = TTF_OpenFont("assets/font.ttf", 20)), TTF_GetError());
}

// Utility.
//----------------------------------------------------------------------------------------------------------------------
#define array_len(array) (sizeof(array) / sizeof(*array))

bool is_inside_rect(SDL_Rect r, SDL_Point p)
{
  return (r.x <= p.x && p.x <= r.x + r.w)
      && (r.y <= p.y && p.y <= r.y + r.h);
}

// Colors and Spheres.
//----------------------------------------------------------------------------------------------------------------------
#define COLOR_TRANSP 0x00
#define COLOR_OPAQUE 0xFF
#define COLOR_BLACK (SDL_Color) {0x00, 0x00, 0x00, COLOR_OPAQUE}
#define COLOR_WHITE (SDL_Color) {0xFF, 0xFF, 0xFF, COLOR_OPAQUE}
#define COLOR_RED   (SDL_Color) {0xFF, 0x00, 0x00, COLOR_OPAQUE}
#define COLOR_GREEN (SDL_Color) {0x00, 0xFF, 0x00, COLOR_OPAQUE}
#define COLOR_BLUE  (SDL_Color) {0x00, 0x00, 0xFF, COLOR_OPAQUE}
#define COLOR_PINK  (SDL_Color) {0xFF, 0x00, 0xFF, COLOR_OPAQUE}

SDL_Color *sphere_colors = NULL;

void generate_sphere_colors(void)
{
  assert(!sphere_colors);
  sphere_colors = malloc(sizeof(*sphere_colors) * g_max_color);
  SDL_Color base_colors[] = {
    {153,   0,   0, COLOR_OPAQUE},
    {  0, 153,  51, COLOR_OPAQUE},
    { 51, 153, 255, COLOR_OPAQUE},
    {255,   0, 102, COLOR_OPAQUE},
    {  0, 255, 255, COLOR_OPAQUE},
    {255, 204,   0, COLOR_OPAQUE},
    {153,  51, 255, COLOR_OPAQUE},
    {153, 153, 102, COLOR_OPAQUE},
    {153, 255, 153, COLOR_OPAQUE},
    {255, 153, 204, COLOR_OPAQUE},
  };
  int base_colors_count = array_len(base_colors);
  int i;
  for (i = 0; i < fmin(g_max_color, base_colors_count); ++i) // First place the hand selected colors.
    sphere_colors[i] = base_colors[i];
  for (; i < g_max_color; ++i) {// If the user asked for more than base_colors_count, then keep adding random colors.
    SDL_Color color = COLOR_BLACK;
    int dt = 16 * (i / 4);
    color.r += dt + ((i + 0) % 3) * dt;
    color.g += dt + ((i + 1) % 3) * dt;
    color.b += dt + ((i + 2) % 3) * dt;
    sphere_colors[i] = color;
/*
    SDL_Color color = {0, 0, 0, COLOR_OPAQUE};
    color.r = rand() % 256;
    color.g = rand() % 256;
    color.r = rand() % 256;
    sphere_colors[i] = color;
*/
/*
    unsigned int r = rand();
    SDL_Color color = {0, 0, 0, COLOR_OPAQUE};
    color.r = (r & 0xFF000000) >> 24;
    color.g = (r & 0x00FF0000) >> 16;
    color.r = (r & 0x0000FF00) >>  8;
    sphere_colors[i] = color;
*/
  }
}

void draw_sphere(SDL_Color c, SDL_Rect *dst)
{
  static SDL_Texture *white_sphere = NULL;
  if (!white_sphere) white_sphere = IMG_LoadTexture(g_renderer, "assets/sphere.png");

  // Change white sphere to color c.
  checked((SDL_SetTextureColorMod(white_sphere, c.r, c.g, c.b) < 0), SDL_GetError());
  if (c.a != COLOR_OPAQUE) checked((SDL_SetTextureAlphaMod(white_sphere, c.a) < 0), SDL_GetError());

  // Draw the sphere at dst.
  checked((SDL_RenderCopy(g_renderer, white_sphere, NULL, dst) < 0), SDL_GetError());
}

SDL_Texture *create_gradient(SDL_Color from, SDL_Color to)
{
  int x = 0, y = 0, w = WINDOW_W, h = WINDOW_H;
  int dr = to.r - from.r, dg = to.g - from.g, db = to.b - from.b;
  int dmax = fmax(fmax(abs(dr), abs(dg)), abs(db)); // Max distance of all color bands.
  double block_size = (double)(h) / (double)(dmax); // The size of each block with the same color.
  SDL_Texture *gradient = NULL;
  checked(
    !(gradient = SDL_CreateTexture(g_renderer, SDL_PIXELFORMAT_RGBA32, SDL_TEXTUREACCESS_TARGET, w, h)),
    SDL_GetError());
  checked((SDL_SetRenderTarget(g_renderer, gradient) < 0), SDL_GetError());
  for (int i = 0; i < dmax; ++i) {
    SDL_Rect block = {.x = x, .y = y + (int)(block_size * i), .w = w, .h = (int)(block_size + 1)};
    SDL_Color c = {
      (uint8_t)(from.r + ((dr * i) / dmax)),
      (uint8_t)(from.g + ((dg * i) / dmax)),
      (uint8_t)(from.b + ((db * i) / dmax)),
      COLOR_OPAQUE
    };
    checked((SDL_SetRenderDrawColor(g_renderer, c.r, c.g, c.b, c.a) < 0), SDL_GetError());
    checked((SDL_RenderFillRect(g_renderer, &block) < 0), SDL_GetError());
  }
  return gradient;
}

// Guess grid.
//----------------------------------------------------------------------------------------------------------------------
void guess_grid_draw(void)
{
  for (int i = 0; i < g_last_guess; ++i) {// Draw guesses bottom-up.
    SDL_Rect guess_try_dst = {
      .x = 0,
      .y = (WINDOW_H * 9) / 10 - SPHERE_DIM * i,
    };
    // Determine guess_try_dst's width and height using SDL_QueryTexture().
    checked((SDL_QueryTexture(g_guess_grid[i], NULL, NULL, &guess_try_dst.w, &guess_try_dst.h) < 0), SDL_GetError());
    checked((SDL_SetRenderTarget(g_renderer, NULL) < 0), SDL_GetError());
    checked((SDL_RenderCopy(g_renderer, g_guess_grid[i], NULL, &guess_try_dst) < 0), SDL_GetError());
  }
}

SDL_Texture *render_guess(Hint hint)
{
  SDL_Texture *guess_texture = NULL;
  int w = SPHERE_DIM * g_passwd_sz + WINDOW_W / 10;
  int h = SPHERE_DIM;
  checked(
    !(guess_texture = SDL_CreateTexture(g_renderer, SDL_PIXELFORMAT_RGBA32, SDL_TEXTUREACCESS_TARGET, w, h)),
    SDL_GetError());
  checked((SDL_SetRenderTarget(g_renderer, guess_texture) < 0), SDL_GetError());
  checked((SDL_SetTextureBlendMode(guess_texture, SDL_BLENDMODE_BLEND) < 0), SDL_GetError());

  // Draw box.
  checked((SDL_SetRenderDrawColor(g_renderer, 64, 64, 64, 64) < 0), SDL_GetError());
  checked((SDL_RenderFillRect(g_renderer, NULL) < 0), SDL_GetError());
  checked((SDL_SetRenderDrawColor(g_renderer, 0, 0, 0, COLOR_OPAQUE) < 0), SDL_GetError());
  checked((SDL_RenderDrawRect(g_renderer, NULL) < 0), SDL_GetError());

  // Draw hints.
  SDL_Rect hints_box = {.x = 0, .y = 0, .w = WINDOW_W / 10, .h = SPHERE_DIM};
  for (int i = 0; i < hint.correct; ++i) {// Correct.
    int scaled_sphere_dim = hints_box.w / g_passwd_sz;
    SDL_Rect hint_dst = {
      .x = i * scaled_sphere_dim,
      .y = SPHERE_DIM / 2 - scaled_sphere_dim,
      .w = scaled_sphere_dim, .h = scaled_sphere_dim
    };
    draw_sphere(COLOR_BLACK, &hint_dst);
  }
  for (int i = 0; i < hint.partial; ++i) {// Partial.
    int scaled_sphere_dim = hints_box.w / g_passwd_sz;
    SDL_Rect hint_dst = {
      .x = i * scaled_sphere_dim,
      .y = SPHERE_DIM / 2 - scaled_sphere_dim + SPHERE_DIM / 2,
      .w = scaled_sphere_dim, .h = scaled_sphere_dim
    };
    draw_sphere(COLOR_WHITE, &hint_dst);
  }

  for (int i = 0; i < g_current_guess_len; ++i) {// Draw guess spheres.
    SDL_Rect dst = {.x = hints_box.w + i * SPHERE_DIM, .y = 0, .w = SPHERE_DIM, .h = SPHERE_DIM};
    draw_sphere(sphere_colors[g_current_guess[i]], &dst);
  }
  return guess_texture;
}

// Current guess.
//----------------------------------------------------------------------------------------------------------------------
SDL_Rect g_guess_accept_dst = {
  .x = (WINDOW_W * 1) / 10,
  .y = (WINDOW_H * 1) / 10 + SPHERE_DIM,
  .w = 100, .h = 50
};
SDL_Rect g_guess_cancel_dst = {
  .x = (WINDOW_W * 1) / 10 + 150,
  .y = (WINDOW_H * 1) / 10 + SPHERE_DIM,
  .w = 100, .h = 50
};

void current_guess_draw(void)
{
  static SDL_Texture *accept_texture = NULL;
  static SDL_Texture *cancel_texture = NULL;

  SDL_Rect current_guess_box = {
    .x = (WINDOW_W * 1) / 10,
    .y = (WINDOW_H * 1) / 10,
    .w = SPHERE_DIM * g_passwd_sz,
    .h = SPHERE_DIM,
  };

  checked((SDL_SetRenderDrawColor(g_renderer, 64, 64, 64, 64) < 0), SDL_GetError());
  checked((SDL_RenderFillRect(g_renderer, &current_guess_box) < 0), SDL_GetError());
  checked((SDL_SetRenderDrawColor(g_renderer, 0, 0, 0, COLOR_OPAQUE) < 0), SDL_GetError());
  checked((SDL_RenderDrawRect(g_renderer, &current_guess_box) < 0), SDL_GetError());

  SDL_Rect sphere_dst = current_guess_box;
  sphere_dst.w = SPHERE_DIM;
  for (int i = 0; i < g_current_guess_len; ++i) {
    draw_sphere(sphere_colors[g_current_guess[i]], &sphere_dst);
    sphere_dst.x += SPHERE_DIM;
  }
  // Draw accept/cancel buttons.
  if (!accept_texture) {// Render accept text.
    SDL_Surface *surface = NULL;
    SDL_Color color = COLOR_BLACK;

    checked(!(surface = TTF_RenderUTF8_Solid(g_font, "Accept", color)), TTF_GetError());
    checked(!(accept_texture = SDL_CreateTextureFromSurface(g_renderer, surface)), SDL_GetError());
    SDL_FreeSurface(surface);
  }
  if (!cancel_texture) {// Render cancel text.
    SDL_Surface *surface = NULL;

    checked(!(surface = TTF_RenderUTF8_Solid(g_font, "Cancel", COLOR_BLACK)), TTF_GetError());
    checked(!(cancel_texture = SDL_CreateTextureFromSurface(g_renderer, surface)), SDL_GetError());
    SDL_FreeSurface(surface);
  }
  checked((SDL_SetRenderTarget(g_renderer, NULL) < 0), SDL_GetError());
  checked((SDL_RenderCopy(g_renderer, accept_texture, NULL, &g_guess_accept_dst) < 0), SDL_GetError());
  checked((SDL_RenderCopy(g_renderer, cancel_texture, NULL, &g_guess_cancel_dst) < 0), SDL_GetError());
}

bool current_guess_handle_input(SDL_Event *event)
{
  if (event->type == SDL_MOUSEBUTTONDOWN) {
    SDL_Point click = {event->button.x, event->button.y};
    if (is_inside_rect(g_guess_accept_dst, click)) {// Accept guess.
      if (g_current_guess_len == g_passwd_sz) {// Only allow accept when guess is full.
        Hint hint = verify(g_passwd, g_current_guess);
        g_guess_grid[g_last_guess++] = render_guess(hint);
        g_current_guess_len = 0;
        if (hint.correct == g_passwd_sz) {// Correct password.
          SDL_ShowSimpleMessageBox(
            SDL_MESSAGEBOX_INFORMATION,
            "Parabens, você ganhou!!!", "Jogue novamente e tente superar seu recorde ;)", NULL);
          exit(0);
        }
        if (g_last_guess == g_max_tries) {// Game over.
          SDL_ShowSimpleMessageBox(
            SDL_MESSAGEBOX_INFORMATION,
            "Tente outra vez", "Que pena, acabaram suas tentativas...", NULL);
          exit(0);
        }
      }
      return true;
    } else if (is_inside_rect(g_guess_cancel_dst, click)) {// Accept guess.
      g_current_guess_len = 0;
      return true;
    }
  }
  return false;
}

// Guess selection.
//----------------------------------------------------------------------------------------------------------------------
SDL_Rect guess_selection_box = {
  .x = (WINDOW_W * 3) / 4,
  .y = (WINDOW_H * 2) / 10,
  .w = SPHERE_DIM * 4,
  .h = SPHERE_DIM * 9,
};

void guess_selection_draw(void)
{
  // Draw sphere box.
  checked((SDL_SetRenderDrawColor(g_renderer, 64, 64, 64, 64) < 0), SDL_GetError());
  checked((SDL_RenderFillRect(g_renderer, &guess_selection_box) < 0), SDL_GetError());
  checked((SDL_SetRenderDrawColor(g_renderer, 0, 0, 0, COLOR_OPAQUE) < 0), SDL_GetError());
  checked((SDL_RenderDrawRect(g_renderer, &guess_selection_box) < 0), SDL_GetError());

  // Draw the spheres.
  SDL_Rect dst = {
    .x = guess_selection_box.x,
    .y = guess_selection_box.y + guess_selection_box.h - SPHERE_DIM,
    .w = SPHERE_DIM, .h = SPHERE_DIM
  };
  for (int i = 0; i < g_max_color; ++i) {
    draw_sphere(sphere_colors[i], &dst);
    if ((dst.x += SPHERE_DIM) >= guess_selection_box.x + guess_selection_box.w) {
      dst.x = guess_selection_box.x;
      dst.y -= SPHERE_DIM;
    }
  }
}

bool guess_selection_handle_input(SDL_Event *event)
{
  if (event->type == SDL_MOUSEBUTTONDOWN) {
    SDL_Point click = {event->button.x, event->button.y};
    if (is_inside_rect(guess_selection_box, click)) {// Select a color.
      if (g_current_guess_len == g_passwd_sz) return true;

      SDL_Rect dst = {
        .x = guess_selection_box.x,
        .y = guess_selection_box.y + guess_selection_box.h - SPHERE_DIM,
        .w = SPHERE_DIM, .h = SPHERE_DIM
      };
      for (int i = 0; i < g_max_color; ++i) {
        if (is_inside_rect(dst, click)) {
          g_current_guess[g_current_guess_len++] = i;
          return true;
        }
        if ((dst.x += SPHERE_DIM) >= guess_selection_box.x + guess_selection_box.w) {
          dst.x = guess_selection_box.x;
          dst.y -= SPHERE_DIM;
        }
      }
    }
  }
  return false;
}

// Main loop.
//----------------------------------------------------------------------------------------------------------------------
void redraw(void)
{
  // Background.
  static SDL_Texture *gradient = NULL;
  if (!gradient) gradient = create_gradient(COLOR_PINK, COLOR_WHITE);
  checked((SDL_SetRenderTarget(g_renderer, NULL) < 0), SDL_GetError());
  checked((SDL_RenderCopy(g_renderer, gradient, NULL, NULL) < 0), SDL_GetError());

  current_guess_draw();
  guess_grid_draw();
  guess_selection_draw();

  // Present drawing.
  SDL_RenderPresent(g_renderer);
}

void handle_input(void)
{
  SDL_Event event;
  while (SDL_PollEvent(&event)) {
    if (event.type == SDL_QUIT) exit(0);
    else if (event.type == SDL_MOUSEBUTTONDOWN) {
      if (current_guess_handle_input(&event)) continue;
      if (guess_selection_handle_input(&event)) continue;
    }
  }
}

void gui_play(int passwd[g_passwd_sz])
{
  g_passwd = passwd;
  gui_init();
  generate_sphere_colors();
  g_current_guess = malloc(sizeof(*g_current_guess) * g_passwd_sz);
  g_guess_grid = malloc(sizeof(*g_guess_grid) * g_max_tries);
  while (true) {
    handle_input();
    redraw();
  }
  free(g_current_guess);
  for (int i = 0; i < g_last_guess; ++i)
    SDL_DestroyTexture(g_guess_grid[i]);
  free(g_guess_grid);
}
