#include <stdio.h>
#include <stdlib.h>
#include <time.h>

typedef struct Slist Slist;
struct Slist {
  void *data;
  Slist *next;
};

void slist_foreach(Slist *head, void (*func)(void *))
{
  if (!head) return;
  func(head->data);
  slist_foreach(head->next, func);
}

Slist *slist_create(void *data)
{
  Slist *l = malloc(sizeof(*l));
  l->data = data;
  l->next = NULL;
  return l;
}

void slist_destroy(Slist **head)
{
  void destroy_rec(Slist *l)
  {
    if (l != NULL) destroy_rec(l->next);
    free(l);
  }
  *head = NULL;
}


Slist *slist_append(Slist *head, Slist *new)
{
  if (!head) return new;
  head->next = slist_append(head->next, new);
  return head;
}

int slist_count(Slist *head)
{
  int count_rec(Slist *head, int count)
  {
    if (!head) return count;
    return count_rec(head->next, count + 1);
  }
  return count_rec(head, 0);
}

Slist *slist_reverse(Slist *head)
{
  Slist *new_head = NULL;
  void reverse_rec(Slist *h, Slist *t)
  {
    if (t->next) reverse_rec(t, t->next);
    else new_head = t;
    t->next = h;
  }
  reverse_rec(head, head->next);
  head->next = NULL;
  return new_head;
}

#define array_len(array) (sizeof(array) / sizeof(*array))

void print_long_list(Slist *l)
{
  void print_long(void *p)
  {
    long l = (long)p;
    printf("[%ld]->", l);
  }
  slist_foreach(l, print_long);
  printf("//\n");
}

void main(int argc, char **argv)
{
  long array_a[] = {50, 1, 12, 13, 14, 2, 3, 15, 20, 16, 8, 25, 4, 55, 0};
  Slist *la = NULL;
  for (int i = 0; i < array_len(array_a); ++i)
    la = slist_append(slist_create((void *)array_a[i]), la);
  la = slist_reverse(la);
  print_long_list(la);

  long array_b[] = {1, 1, 2, 3, 5, 8, 13, 21, 34, 55};
  Slist *lb = NULL;
  for (int i = 0; i < array_len(array_b); ++i)
    lb = slist_append(slist_create((void *)array_b[i]), lb);
  lb = slist_reverse(lb);
  print_long_list(lb);

  la = slist_append(la, lb);
  print_long_list(la);
  print_long_list(lb);

  slist_destroy(&la);
  print_long_list(la);

  putchar('\n');
}
