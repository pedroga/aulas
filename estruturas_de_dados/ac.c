#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

// Array circular com funcionalidades de deque.

typedef struct {
  int first, len, cap;
  int data[];
} Ac;

Ac *ac_create(int cap)
{
  Ac *ac = malloc(sizeof(*ac) + sizeof(*ac->data) * cap);
  ac->first = ac->len = 0;
  ac->cap = cap;
  return ac;
}

void ac_push_back(Ac *ac, int val)
{
  if (ac->len >= ac->cap) return; // A lista esta cheia.
  ac->data[(ac->first + ac->len++) % ac->cap] = val;
}

void ac_push_front(Ac *ac, int val)
{
  if (ac->len >= ac->cap) return; // A lista esta cheia.
  if (--ac->first < 0) ac->first = ac->cap - 1;
  ac->data[ac->first] = val;
  ++ac->len;
}

void ac_pop_back(Ac *ac)
{
  if (ac->len == 0) return; // A lista já está vazia.
  --ac->len;
}

void ac_pop_front(Ac *ac)
{
  if (ac->len == 0) return; // A lista já está vazia.
  ac->first = (ac->first + 1) % ac->cap;
  --ac->len;
}

int ac_at(Ac *ac, int idx)
{
  assert(0 <= idx && idx < ac->len && "Idx out of bounds");
  return ac->data[(ac->first + idx) % ac->cap];
}

void ac_print(Ac *ac)
{
  for (int i = ac->first, count = 0; count < ac->len; ++i, ++count)
    printf("[%d]", ac->data[i % ac->cap]);
  putchar('\n');
}

int main()
{
  Ac *ac = ac_create(10);
  ac_print(ac);

  ac_push_front(ac, 3);
  ac_print(ac);

  ac_push_front(ac, 6);
  ac_print(ac);

  ac_push_back(ac, 7);
  ac_push_back(ac, 8);
  ac_print(ac);

  ac_pop_front(ac);
  ac_print(ac);

  ac_push_front(ac, 9);
  ac_pop_back(ac);
  ac_push_back(ac, 10);
  ac_print(ac);

  for (int i = 0; i < ac->len; ++i)
    printf("at[%d] = %d\n", i, ac_at(ac, i));

  puts("Real:");
  for (int i = 0; i < ac->cap; ++i)
    printf("[%d] = %d\n", i, ac->data[i]);
}
