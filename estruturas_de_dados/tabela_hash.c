#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

typedef struct aluno {
  int matricula;
  char nome[50];
} Aluno;

int hash_nome(Aluno aluno)
{
  int sum = 0;
  for (int i = 0; aluno.nome[i]; ++i)
    sum += aluno.nome[i];
  return sum;
}

int hash_matricula(Aluno aluno)
{
  return aluno.matricula;
}

typedef int (*hash_func)(Aluno aluno);

typedef struct tab_aluno {
  hash_func hash;
  int max;
  Aluno data[];
} Tab_aluno;

Tab_aluno *tab_create(int max, hash_func hash)
{
  Tab_aluno *tab = malloc(sizeof(*tab) + sizeof(*tab->data) * max);
  tab->max = max;
  tab->hash = hash;
  for (int i = 0; i < max; ++i)
    tab->data[i].matricula = -1;
  return tab;
}

void tab_insert(Tab_aluno *tab, Aluno aluno)
{
  int idx = tab->hash(aluno) % tab->max;
  int last_idx = idx - 1 < 0 ? tab->max - 1 : idx - 1;
  while (tab->data[idx % tab->max].matricula != -1 && idx != last_idx)
    ++idx;
  if (idx == last_idx) return; // Tabela cheia.
  tab->data[idx] = aluno;
}

bool cmp_aluno(Aluno a, Aluno b)
{
  return a.matricula == b.matricula && !strcmp(a.nome, b.nome);
}

int tab_lookup(Tab_aluno *tab, Aluno aluno)
{
  int idx = tab->hash(aluno);
  while (!cmp_aluno(tab->data[idx % tab->max], aluno))
    ++idx;
  return idx % tab->max;
}

void tab_remove(Tab_aluno *tab, Aluno aluno)
{
  int idx = tab_lookup(tab, aluno);
  tab->data[idx].matricula = -1;
}

void print_aluno(Aluno aluno)
{
  printf("{ .matricula = %d, .nome = [%s] }\n", aluno.matricula, aluno.nome);
}

int main()
{
  Tab_aluno *tab_m = tab_create(23, hash_matricula);
  Tab_aluno *tab_n = tab_create(23, hash_nome);
  Aluno alunos[10] = {
    {.matricula = 1001, .nome = "João"},
    {.matricula = 1101, .nome = "Alice"},
    {.matricula = 1011, .nome = "Mateus"},
    {.matricula = 1203, .nome = "Lucas"},
    {.matricula = 4307, .nome = "Tiago"},
    {.matricula = 0110, .nome = "Maria"},
    {.matricula = 5078, .nome = "Ana"},
    {.matricula = 6007, .nome = "Leticia"},
    {.matricula = 4213, .nome = "Leonardo"},
    {.matricula = 1432, .nome = "Mariana"},
  };
  for (int i = 0; i < 10; ++i) {
    tab_insert(tab_m, alunos[i]);
    tab_insert(tab_n, alunos[i]);
  }
  puts("Por matricula");
  for (int i = 0; i < tab_m->max; ++i) {
    printf("[%d] = ", i);
    print_aluno(tab_m->data[i]);
  }
  puts("Por nome");
  for (int i = 0; i < tab_n->max; ++i) {
    printf("[%d] = ", i);
    print_aluno(tab_n->data[i]);
  }
  for (int i = 0; i < 10; ++i)
    printf("O aluno [%d] esta no indice %d de tab_m e no indice %d de tab_n\n",
           i, tab_lookup(tab_m, alunos[i]), tab_lookup(tab_n, alunos[i]));
}
