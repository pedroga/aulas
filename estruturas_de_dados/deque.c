// Deque (double ended queue)

#include <assert.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

// Interface.
//--------------------------------------------------------------------------------------------------------------------;
typedef struct node {
  int val;
  struct node *next, *prev;
} Node;

typedef struct {
  int len;
  Node *first, *last;
} Deque;

Deque deque_create(void);
void deque_destroy(Deque *deque);
bool deque_empty(Deque *deque);

void deque_push_back(Deque *deque, int val);
int deque_back(Deque *deque);
void deque_pop_back(Deque *deque);

void deque_push_front(Deque *deque, int val);
int deque_front(Deque *deque);
void deque_pop_front(Deque *deque);

int *deque_at(Deque *deque, int idx);
void deque_foreach(Deque *deque, void (*func)(int i));

// Implementação.
//--------------------------------------------------------------------------------------------------------------------;
Deque deque_create(void)
{
  return (Deque) {.len = 0, .first = NULL, .last = NULL};
}

void deque_node_destroy(Node *node)
{
  assert(node);
  if (node->next) deque_node_destroy(node->next);
  free(node);
}

void deque_destroy(Deque *deque)
{
  if (!deque_empty(deque)) deque_node_destroy(deque->first);
  deque->first = deque->last = NULL;
  deque->len = 0;
}

bool deque_empty(Deque *deque)
{
  return deque->len == 0;
}

void deque_push_back(Deque *deque, int val)
{
  Node *node = malloc(sizeof(*node)); // Cria novo nó da lista ligada.
  *node = (Node) {.val = val, .next = NULL, .prev = NULL};
  if (deque_empty(deque)) {// NULL <- novo_nó -> NULL.
    deque->first = deque->last = node;
  } else {// NULL <- primeiro_nó <-> nó <-> ... <-> nó <-> último_nó <-> novo_nó -> NULL.
    // Precisamos de três operações para inserir o novo nó.
    node->prev = deque->last; // Primeiramente, novo_nó.prev -> último_nó.
    deque->last->next = node; // Segundamente, último_nó.next -> novo_nó.
    deque->last = node; // Por fim, novo_nó se torna último_nó.
  }
  ++deque->len;
}

int deque_back(Deque *deque)
{
  assert("The list is empty" && !deque_empty(deque));
  return deque->last->val;
}

void deque_pop_back(Deque *deque)
{
  assert("The list is empty" && !deque_empty(deque));
  Node *back = deque->last; // Copiamos um ponteiro para o último elemento.
  deque->last = back->prev; // O penúltimo elemento se torna o último.
  // Note que se a lista tinha apenas um elemento, agora deque->last = NULL.
  deque->last->next = NULL; // Novo último_nó.next passa a apontar para NULL.
  free(back);
  --deque->len;
}

void deque_push_front(Deque *deque, int val)
{
  Node *node = malloc(sizeof(*node)); // Cria novo nó da lista ligada.
  *node = (Node) {.val = val, .next = NULL, .prev = NULL};
  if (deque_empty(deque)) {// NULL <- novo_nó -> NULL.
    deque->first = deque->last = node;
  } else {// NULL <- novo_nó <-> primeiro_nó <-> nó <-> ... <-> nó <-> último_nó -> NULL.
    // Precisamos de três operações para inserir o novo nó.
    node->next = deque->first; // Primeiramente, novo_nó.next -> primeiro_nó.
    deque->first->prev = node; // Segundamente, primeiro_nó.prev -> novo_nó.
    deque->first = node; // Por fim, novo_nó se torna primeiro_nó.
  }
  ++deque->len;
}

int deque_front(Deque *deque)
{
  assert("The list is empty" && !deque_empty(deque));
  return deque->first->val;
}

void deque_pop_front(Deque *deque)
{
  assert("The list is empty" && !deque_empty(deque));
  Node *front = deque->first; // Copiamos um ponteiro para o primeiro elemento.
  deque->first = front->next; // O segundo elemento se torna o primeiro.
  // Note que se a lista tinha apenas um elemento, agora deque->first = NULL.
  deque->first->prev = NULL; // Novo primeiro_nó.prev passa a apontar para NULL.
  free(front);
  --deque->len;
}

int *deque_node_at(Node *node, int idx)
{
  if (idx) return deque_node_at(node->next, idx - 1);
  return &node->val;
}

int *deque_at(Deque *deque, int idx)
{
  assert("The index is out of bounds" && (0 < idx && idx < deque->len));
  return deque_node_at(deque->first, idx);
}

void deque_foreach(Deque *deque, void (*func)(int i))
{
  for (Node *it = deque->first; it; it = it->next) {
    func(it->val);
  }
}

// Teste.
//----------------------------------------------------------------------------------------------------------------------
void print_elem(int i)
{
  printf("[%d]", i);
}

int main()
{
  printf("DEQUE TEST\n");
  Deque deque = deque_create();

  for (int i = 0; i < 10; ++i)
    deque_push_back(&deque, i);
  *deque_at(&deque, 9) = 42;
  deque_foreach(&deque, print_elem); putchar('\n');
  printf("deque_back = %d\n", deque_back(&deque));

  for (int i = 0; i < 10; ++i)
    deque_push_back(&deque, i);
  deque_foreach(&deque, print_elem); putchar('\n');
  printf("deque_back = %d\n", deque_back(&deque));

  deque_pop_back(&deque);
  deque_pop_front(&deque);
  deque_foreach(&deque, print_elem); putchar('\n');
  printf("deque_back = %d\n", deque_back(&deque));
  printf("deque_front = %d\n", deque_front(&deque));

  deque_destroy(&deque);
  printf("Deque after destroy: ");
  deque_foreach(&deque, print_elem); putchar('\n');

  return 0;
}
