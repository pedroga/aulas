#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

bool esta_balanceado(char *expr)
{
  char *pilha = malloc(strlen(expr));
  int topo = 0;
  bool result = true;

  for (int i = 0; expr[i]; ++i) {
    // Empilha.
    switch (expr[i]) {
    case '(': case '[': case '{':
      pilha[topo++] = expr[i];
      break;
      // case para desempilhar.
      // Se tentou desempilhar e topo == 0, então esta desbalanceado.
      // Se tentou desempilhar expr[i] == ')' e pilha[topo] != '(', então esta desbalanceado.
    default:
      printf("Caractere inválido %c\n", expr[i]);
      abort();
    }
  }
  return topo == 0;
}

int main()
{
  char *exprs_balanceadas[] = {
    "(())[]{}",
    "({}[])",
    "{{[](())}[]}",
  };
  char *exprs_desbalanceadas[] = {
    "(()))",
    "[]{}()({)}",
    "[[]",
  };
  for (int i = 0; i < 3; ++i) {
    printf("Esta balanceado = %d\n", esta_balanceado(exprs_balanceadas[i]));
    printf("Esta balanceado = %d\n", esta_balanceado(exprs_desbalanceadas[i]));
  }
}
